import './assets/styles/index.scss';

const coverBtn = document.querySelector('.cover-btn');
const banner = document.querySelector('.banner');
const content = document.querySelector('.content');

coverBtn.addEventListener('click', e => {
    if (banner.classList.contains('banner-cover')) {
        banner.classList.remove('banner-cover');
        content.classList.remove('content-hide');
    } else {
        banner.classList.add('banner-cover');
        content.classList.add('content-hide');
    }
});

const burguer = document.querySelector('.navbar-burguer-container');
const fsMenu = document.querySelector('.full-screen-menu');

burguer.addEventListener('click', e => {
    const { currentTarget } = e;
    if (currentTarget.classList.contains('active')) {
        currentTarget.classList.remove('active');
        fsMenu.classList.remove('active');
    } else {
        currentTarget.classList.add('active');
        fsMenu.classList.add('active');
    }
});